Rails.application.routes.draw do
  root 'home#index'
  get 'dash' => 'dashboard#index'
  get 'home/index'
  # resources :dashboard
  resources :phones
  resources :addresses
  resources :contacts
  resources :kinds
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end